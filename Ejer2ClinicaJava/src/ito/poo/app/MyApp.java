package ito.poo.app;
import java.time.LocalDate;
import Clinica.Expediente;

public class MyApp {

	public static void main(String[] args) {
		
		Expediente composicion1;
		composicion1=new Expediente("Cecilia Martinez Cruz", 1234, LocalDate.of(0, null, 0), "Usted se encuentra bien", LocalDate.now(), "fiebre", "saludable", "paracetamol", 38.4f, 56.7f, 1.70f, "dolor de garganta");
	    System.out.println(composicion1);
	}
}
